﻿using Terraria.ID;
using Terraria.ModLoader;
using Terraria;

namespace NoTextPulse {
	public class NoTextPulse : Mod {
		public NoTextPulse() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		public override void Load() {
			Main.mouseTextColorChange = 0;
			Main.cursorColorDirection = 0;
		}

		public override void Unload() {
			Main.mouseTextColorChange = 1;
			Main.cursorColorDirection = 1;
		}
		
		public override void PostUpdateInput() {
			Main.mouseTextColorChange = 0;
			Main.mouseTextColor = 255;
			Main.cursorColorDirection = 0;
			Main.cursorAlpha = 1f;
		}
	}
}